import calendar
import csv
import datetime
import decimal
import io
import json
import re
from typing import Dict, List, Match, Optional, cast

import pytz
from dateutil.parser import parse

from reporting.encoders import DataClassEncoder

TIMEZONE = pytz.timezone('America/Los_Angeles')
MEMBERSHIP_LEVEL_REMAPPING: Dict[str, str] = {
    'Cardinal & Gray': 'Life',
    'Senior Alumnus': 'Life',
    'Regular': 'Sustaining',
    'Recent Grad': 'MIT10',
}


def clean_membership_level(s: str) -> str:
    """
    Cleans the extraneous bits of the membership level, and maps to a canonical value.

    Output should be one of:
        - Sustaining (mapped from Regular)
        - Life (mapped from Cardinal & Gray, Senior Alumnus)
        - MIT10 (mapped from Recent Grad)
        - Student
        - Patron
        - Benefactor
        - Sponsor
    """
    match = cast(Match[str], re.search(r'(?:MEM - C?NC - )?([\w &]+)(?: \(.*\))?', s))
    cleaned = match.group(1)
    cleaned = cleaned.replace(' and ', ' & ')
    cleaned = cleaned.strip()

    return MEMBERSHIP_LEVEL_REMAPPING.get(cleaned, cleaned)


def clean_boolean(s: str) -> bool:
    s = s.lower()
    return s in ('true', '1')


def clean_datetime(s: Optional[str]) -> Optional[int]:
    if s is None:
        return None

    try:
        dt = parse(s)
        if not dt.tzinfo:
            dt = TIMEZONE.localize(dt)
        dt = dt.astimezone(pytz.utc)
        return int(dt.timestamp())
    except ValueError:
        # TODO Log error
        pass

    return None


def strip_time_from_timestamp(timestamp: int) -> int:
    dt = datetime.datetime.fromtimestamp(timestamp)
    dt = TIMEZONE.localize(dt)
    dt = dt.astimezone(pytz.utc)
    return int(calendar.timegm(dt.date().timetuple()))


def clean_decimal(s: Optional[str]) -> Optional[decimal.Decimal]:
    return decimal.Decimal(s) if s else None


def clean_string(s: str) -> Optional[str]:
    return s.strip() or None


def clean_cybersource_transaction_id(s: str) -> Optional[str]:
    cleaned = clean_string(s)
    return cleaned if (cleaned and re.match(r'\d+', cleaned)) else None


def dataclass_list_to_csv(data: List) -> io.StringIO:
    """ Converts a list of dataclass instances to a CSV string buffer. """
    buffer = io.StringIO()

    if len(data) > 0:
        keys = data[0].__dataclass_fields__

        writer = csv.DictWriter(buffer, fieldnames=keys, quoting=csv.QUOTE_ALL)
        writer.writeheader()

        rows = map(lambda d: d.__dict__, data)
        writer.writerows(rows)

    buffer.seek(0)
    return buffer


def dataclass_list_to_json(data: List) -> io.StringIO:
    """ Converts a list of dataclass instances to a string buffer where each
    instance is JSON-encoded on its own line.

    Note: The output is NOT as JSON array, but similar to a CSV where each line
        represents a single instance.
    """
    json_str = '\n'.join(json.dumps(datum, cls=DataClassEncoder) for datum in data)
    buffer = io.StringIO(json_str)
    buffer.seek(0)
    return buffer
