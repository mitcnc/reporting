import csv
import decimal
import io
from typing import Optional, Tuple

from peewee import DoesNotExist
from playhouse.dataset import DataSet

from reporting.models import DATABASE, Membership, Purchase, db
from reporting.utils import (
    clean_boolean, clean_cybersource_transaction_id, clean_datetime, clean_decimal, clean_membership_level,
    clean_string, strip_time_from_timestamp
)

ALUM_EMAIL_DOMAIN = 'alum.mit.edu'


def get_username(username: Optional[str], email: Optional[str]) -> Optional[str]:
    if username:
        return username.lower().strip()

    email = email or ''
    email = email.lower().strip()

    suffix = f'@{ALUM_EMAIL_DOMAIN}'
    if email.endswith(suffix):
        return email.strip(suffix)

    return None


def upsert_membership_row(purchase: Purchase) -> None:
    # TODO Handle mid-cycle upgrades (e.g., Sustaining --> Patron)
    # NOTE: We use the date (without time) to simplify duplicate determination.
    #   None of the decisions we are making in the data warehouse should require
    #   lower granularity.
    purchased_at = strip_time_from_timestamp(purchase.purchased_at)
    cybersource_transaction_id = purchase.cybersource_transaction_id
    expires_at = None
    if purchase.expires_at and purchase.level != 'Life':
        expires_at = strip_time_from_timestamp(purchase.expires_at)

        if expires_at < purchased_at:
            print(f'Purchase dates are invalid: commerce_instance_uid={purchase.commerce_instance_uid}, '
                  f'purchased_at={purchase.purchased_at}, expires_at={purchase.expires_at}')
            return

    try:
        # Check if the new purchase data extends the existing membership start or end dates.
        query = Membership.select().where(
            (Membership.advance_id == purchase.advance_id) &
            (Membership.level == purchase.level) &
            (
                Membership.ended_at.is_null() |
                (purchased_at <= Membership.ended_at)
            )
        )

        if expires_at:
            query.where(expires_at >= Membership.started_at)

        existing_membership = query.get()
        existing_membership.started_at = min(purchased_at, existing_membership.started_at)
        existing_membership.ended_at = None if existing_membership.ended_at is None or expires_at is None else max(
            expires_at, existing_membership.ended_at)
        if cybersource_transaction_id:
            cybersource_transaction_ids = [cybersource_transaction_id]

            if existing_membership.cybersource_transaction_ids:
                cybersource_transaction_ids = existing_membership.cybersource_transaction_ids.split(',') + \
                    cybersource_transaction_ids

            existing_membership.cybersource_transaction_ids = ','.join(cybersource_transaction_ids)
        existing_membership.save()
    except DoesNotExist:
        Membership.create(
            advance_id=purchase.advance_id,
            username=purchase.username,
            first_name=purchase.first_name,
            last_name=purchase.last_name,
            email=purchase.email,
            level=purchase.level,
            started_at=purchased_at,
            ended_at=expires_at,
            cybersource_transaction_ids=cybersource_transaction_id,
        )


def clean_membership_data(input_file: io.TextIOBase) -> Tuple[io.StringIO, io.StringIO]:
    print('Cleaning iModules data...')

    db.connect()
    db.create_tables([Membership, Purchase])

    # Read the CSV file
    reader = csv.DictReader(input_file)
    for row in reader:
        advance_id = row['ADVANCE_ID']

        # Ignore data without an Advance ID. It's our primary key.
        if not advance_id:
            continue

        # Clean the data
        commerce_instance_uid = clean_string(row['commerce_instance_uid'])
        purchased_at = clean_datetime(row['Purchase Date']) or clean_datetime(row['MEM - CNC - Purchase Date'])
        purchase_price = clean_decimal(row['MEM - CNC - Purchase Price']) or decimal.Decimal(0)
        scholarship_contribution = clean_decimal(
            row['Club of Northern California Membership Registration Form - CNC Scholarship']) or decimal.Decimal(0)
        purchase_promo_code = clean_string(row['MEM - CNC - Promotion Code'])
        expires_at = clean_datetime(row['MEM - CNC - Expiration Date'])
        level = clean_membership_level(row['MEM - CNC - Membership Level'])
        is_lifetime = clean_boolean(row['MEM - CNC - Lifetime'])
        auto_renewal_enabled = clean_boolean(row['MEM - CNC - Auto Renewal'])
        cybersource_transaction_id = clean_cybersource_transaction_id(row['Customer Trans Number'])

        email = clean_string(row['EMAIL_ADDRESS'])
        first_name = clean_string(row['FIRST_NAME'])
        last_name = clean_string(row['LAST_NAME'])
        username = get_username(row['Username'], email)

        try:
            purchase = Purchase.create(
                commerce_instance_uid=commerce_instance_uid,
                advance_id=advance_id,
                first_name=first_name,
                last_name=last_name,
                email=email,
                level=level,
                purchased_at=purchased_at,
                purchase_price=purchase_price,
                scholarship_contribution=scholarship_contribution,
                purchase_promo_code=purchase_promo_code,
                expires_at=expires_at,
                is_lifetime=is_lifetime,
                auto_renew_enabled=auto_renewal_enabled,
                username=username,
                cybersource_transaction_id=cybersource_transaction_id,
            )
            upsert_membership_row(purchase)
        except Exception:
            print(f'Failed to create purchase: commerce_instance_uid={commerce_instance_uid}')

    db.close()

    return _get_cleaned_data()


def _table_to_csv(dataset, query) -> io.StringIO:
    buffer = io.StringIO()
    dataset.freeze(query, format='csv', file_obj=buffer, quoting=csv.QUOTE_ALL)
    buffer.seek(0)
    return buffer


def _get_cleaned_data() -> Tuple[io.StringIO, io.StringIO]:
    dataset = DataSet(f'sqlite:///{DATABASE}')
    membership = _table_to_csv(dataset, Membership.select(
        Membership.advance_id,
        Membership.username,
        Membership.first_name,
        Membership.last_name,
        Membership.email,
        Membership.level,
        Membership.started_at,
        Membership.ended_at,
        Membership.cybersource_transaction_ids,
    ).order_by(Membership.started_at, Membership.advance_id))
    purchases = _table_to_csv(dataset, dataset['purchase'].all())

    return membership, purchases
