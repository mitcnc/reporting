import re
from dataclasses import dataclass, field
from typing import Dict, List, Optional, Union, cast

import environ
import phpserialize
import pymysql


@dataclass
class User:
    id: int
    username: str
    first_name: str
    last_name: str
    email: str
    advance_id: Optional[str] = None
    encrypted_advance_id: Optional[str] = None
    encompass_id: Optional[str] = None
    affiliation: Optional[str] = None
    affiliation_description: Optional[str] = None
    blacklisted: bool = False
    deceased: bool = False
    graduating: bool = False
    micromaster: bool = False
    sloan: bool = False
    class_year: Optional[int] = None
    is_mitaa_club_member: bool = False
    club_ids: List[str] = field(default_factory=list)
    club_names: List[str] = field(default_factory=list)
    stripe_customer_id: Optional[str] = None
    last_login: Optional[int] = None


class WordpressImporter:
    def __init__(self, db_connection_string: str, db_ssl_cert_path: str):
        db_connection_string = db_connection_string
        config = environ.Env.db_url_config(db_connection_string)

        self.db_host = config["HOST"]
        self.db_port = config["PORT"]
        self.db_name = config["NAME"]
        self.db_user = config["USER"]
        self.db_password = config["PASSWORD"]
        self.db_ssl_cert_path = db_ssl_cert_path

    def get_users(self) -> List[User]:
        users = []
        query = """
        WITH
            first_names AS (
                SELECT
                    user_id,
                    meta_value
                FROM wp_usermeta
                WHERE
                    meta_key = 'first_name'
            ),
            last_names AS (
                SELECT
                    user_id,
                    meta_value
                FROM wp_usermeta
                WHERE
                    meta_key = 'last_name'
            ),
            mitaa_metadata AS (
                SELECT
                    user_id,
                    meta_value
                FROM wp_usermeta
                WHERE
                    meta_key = 'mitaa-member_data'
            ),
            stripe_metadata AS (
                SELECT
                    user_id,
                    meta_value
                FROM wp_usermeta
                WHERE
                    meta_key = 'wp-stripe-customer-id'
            ),
            session_metadata AS (
                SELECT
                    user_id,
                    meta_value
                FROM wp_usermeta
                WHERE
                    meta_key = 'session_tokens'
            )
        SELECT
            u.id,
            u.user_nicename,
            u.user_email,
            mm.meta_value AS mitaa_metadata,
            sm.meta_value AS stripe_customer_id,
            ssm.meta_value AS session_metadata,
            fn.meta_value AS first_name,
            ln.meta_value AS last_name
        FROM
            wp_users u
            JOIN mitaa_metadata mm ON u.id = mm.user_id
            LEFT JOIN stripe_metadata sm ON u.id = sm.user_id
            LEFT JOIN session_metadata ssm ON u.id = ssm.user_id
            LEFT JOIN last_names ln ON u.id = ln.user_id
            LEFT JOIN first_names fn ON u.id = fn.user_id
        ORDER BY
            u.id
        """

        conn = pymysql.connect(
            host=self.db_host,
            port=self.db_port,
            database=self.db_name,
            user=self.db_user,
            password=self.db_password,
            ssl_ca=self.db_ssl_cert_path
        )
        cursor = conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute(query)

        # NOTE: Adapted from https://stackoverflow.com/a/1176023/592820
        camel_to_snake = re.compile(r'(?<!^)(?=[A-Z])')

        for row in cursor:
            row = cast(Dict[str, Union[str, int]], row)
            user_id = cast(int, row['id'])
            metadata = {}

            try:
                # NOTE: We need to use encode and decode because phpserialize is not
                # properly updated for Python 3.x.
                mitaa_metadata = cast(str, row['mitaa_metadata']).encode('utf8')
                for key, value in phpserialize.loads(mitaa_metadata).items():
                    key = camel_to_snake.sub('_', key.decode()).lower()
                    if type(value) is bytes:
                        value = value.decode()
                    metadata[key] = value
            except BaseException as e:
                print(f'Failed to de-serialize PHP metadata for user {user_id}. Skipping. Error: {e}')
                continue

            clubs = metadata.pop('clubs', {}).values()
            metadata['is_mitaa_club_member'] = len(clubs) > 0
            metadata['club_ids'] = list(set([club[b'club'].decode() for club in clubs if club[b'club']]))
            metadata['club_names'] = list(set([club[b'clubDesc'].decode() for club in clubs if club[b'clubDesc']]))

            metadata['affiliation_description'] = metadata.pop('affiliation_desc', None)
            metadata['email'] = metadata.pop('email_address', row['user_email'])
            metadata['stripe_customer_id'] = row['stripe_customer_id'] or None

            metadata['username'] = metadata.get('username', row['user_nicename'])
            metadata['first_name'] = metadata.get('first_name', row['first_name'])
            metadata['last_name'] = metadata.get('last_name', row['last_name'])

            if row['session_metadata']:
                session_metadata = cast(str, row['session_metadata']).encode('utf8')
                sessions = phpserialize.loads(session_metadata)
                metadata['last_login'] = max([session[b'login'] for session in sessions.values()])

            user = User(id=user_id, **metadata)
            users.append(user)

        return users
