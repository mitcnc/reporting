"""
This file contains database model definitions. We use these models
to aid in working with a temporary database for data cleansing.
"""
import os

from peewee import BooleanField, CharField, DecimalField, IntegerField, Model, SqliteDatabase

DATABASE: str = '/tmp/reporting.db'

db = SqliteDatabase(DATABASE)


def destroy_database():
    if os.path.exists(DATABASE):
        os.remove(DATABASE)
        print(f'Deleted database: {DATABASE}')
    else:
        print(f'Database {DATABASE} not deleted since it does not exist.')


class BaseModel(Model):
    class Meta:
        database = db


class Membership(BaseModel):
    advance_id = CharField()
    username = CharField(null=True)
    first_name = CharField()
    last_name = CharField()
    email = CharField(null=True)
    level = CharField()
    started_at = IntegerField()
    ended_at = IntegerField(null=True)
    cybersource_transaction_ids = CharField(null=True)


class Purchase(BaseModel):
    commerce_instance_uid = CharField()
    advance_id = CharField()
    username = CharField(null=True)
    first_name = CharField()
    last_name = CharField()
    email = CharField(null=True)
    level = CharField()
    purchased_at = IntegerField()
    purchase_price = DecimalField(decimal_places=2)
    scholarship_contribution = DecimalField(decimal_places=2, null=True)
    purchase_promo_code = CharField(null=True)
    expires_at = IntegerField(null=True)
    is_lifetime = BooleanField()
    auto_renew_enabled = BooleanField()
    cybersource_transaction_id = CharField(null=True)

    class Meta:
        primary_key = False
