import uuid

import pytest
from peewee import SqliteDatabase

from ..imodules import get_username, upsert_membership_row
from ..models import Membership, Purchase
from ..utils import clean_datetime, strip_time_from_timestamp

MODELS = [Membership, Purchase]


def test_get_username():
    username = 'clintonb'
    assert get_username(None, None) is None
    assert get_username('', '') is None

    assert get_username(username, '') == username
    assert get_username('', f'{username}@alum.mit.edu') == username
    assert get_username(username, f'{username}@alum.mit.edu') == username


@pytest.fixture
def db():
    test_db = SqliteDatabase(':memory:')
    test_db.connect()
    with test_db.bind_ctx(MODELS, bind_refs=False, bind_backrefs=False):
        test_db.create_tables(MODELS)
        yield test_db

    test_db.close()


def test_upsert_membership_row(db):
    purchase_dates = [
        ('2019-12-31', '2020-12-31', '123'),
        ('2019-12-31', '2020-12-31', None),
        ('2020-01-01', '2021-01-01', '456'),
        ('2020-11-15', '2021-01-01', '789'),
    ]
    advance_id = '1234'
    username = 'foo'
    email = 'alice@example.com'
    first_name = 'Alice'
    last_name = 'Bob'
    level = 'Sustaining'
    for purchased_at, expires_at, cybersource_transaction_id in purchase_dates:
        purchased_at = clean_datetime(purchased_at)
        expires_at = clean_datetime(expires_at)
        purchase = Purchase.create(
            commerce_instance_uid=str(uuid.uuid4()),
            advance_id=advance_id,
            first_name=first_name,
            last_name=last_name,
            email=email,
            level=level,
            purchased_at=purchased_at,
            purchase_price=50,
            scholarship_contribution=None,
            purchase_promo_code=None,
            expires_at=expires_at,
            is_lifetime=False,
            auto_renew_enabled=False,
            username=username,
            cybersource_transaction_id=cybersource_transaction_id,
        )
        upsert_membership_row(purchase)

    assert Membership.select().where(Membership.advance_id == advance_id).count() == 1

    membership = Membership.get(Membership.advance_id == advance_id)
    assert membership.started_at == 1577750400
    assert membership.ended_at == 1609459200
    assert membership.email == email
    assert membership.username == username
    assert membership.first_name == first_name
    assert membership.last_name == last_name
    assert membership.level == level
    assert membership.cybersource_transaction_ids == '123,456,789'


def test_upsert_membership_row_with_life(db):
    purchase_dates = [
        ('2008-10-07', '2013-09-30',),
        ('2008-10-07', None,),
        ('2008-10-07', None,),
        ('2019-03-31', None,),
    ]
    advance_id = '1234'
    username = 'foo'
    email = 'alice@example.com'
    first_name = 'Alice'
    last_name = 'Bob'
    level = 'Life'
    for purchased_at, expires_at in purchase_dates:
        purchased_at = clean_datetime(purchased_at)
        expires_at = clean_datetime(expires_at)
        purchase = Purchase.create(
            commerce_instance_uid=str(uuid.uuid4()),
            advance_id=advance_id,
            first_name=first_name,
            last_name=last_name,
            email=email,
            level=level,
            purchased_at=purchased_at,
            purchase_price=50,
            scholarship_contribution=None,
            purchase_promo_code=None,
            expires_at=expires_at,
            is_lifetime=False,
            auto_renew_enabled=False,
            username=username,
        )
        upsert_membership_row(purchase)

    assert Membership.select().where(Membership.advance_id == advance_id).count() == 1

    membership = Membership.get(Membership.advance_id == advance_id)
    assert membership.started_at == 1223337600
    assert membership.ended_at is None
    assert membership.email == email
    assert membership.username == username
    assert membership.first_name == first_name
    assert membership.last_name == last_name
    assert membership.level == level


def test_upsert_membership_row_with_multiple_levels(db):
    purchase_dates = [
        ('2018-10-10', '2021-09-22', 'MIT10',),
        ('2018-10-10', '2019-06-30', 'MIT10',),
        ('2021-02-25', '2022-10-29', 'Sustaining',),
    ]
    advance_id = '1234'
    username = 'foo'
    email = 'alice@example.com'
    first_name = 'Alice'
    last_name = 'Bob'
    for purchased_at, expires_at, level in purchase_dates:
        purchased_at = clean_datetime(purchased_at)
        expires_at = clean_datetime(expires_at)
        purchase = Purchase.create(
            commerce_instance_uid=str(uuid.uuid4()),
            advance_id=advance_id,
            first_name=first_name,
            last_name=last_name,
            email=email,
            level=level,
            purchased_at=purchased_at,
            purchase_price=50,
            scholarship_contribution=None,
            purchase_promo_code=None,
            expires_at=expires_at,
            is_lifetime=False,
            auto_renew_enabled=False,
            username=username,
        )
        upsert_membership_row(purchase)

    memberships = list(Membership.select().where(Membership.advance_id == advance_id).order_by(Membership.started_at))
    assert len(memberships) == 2

    membership = memberships[0]
    assert membership.started_at == 1539129600
    assert membership.ended_at == 1632268800
    assert membership.email == email
    assert membership.username == username
    assert membership.first_name == first_name
    assert membership.last_name == last_name
    assert membership.level == 'MIT10'

    membership = memberships[1]
    assert membership.started_at == 1614211200
    assert membership.ended_at == 1667001600
    assert membership.email == email
    assert membership.username == username
    assert membership.first_name == first_name
    assert membership.last_name == last_name
    assert membership.level == 'Sustaining'


def test_upsert_membership_row_with_ignored_purchases(db):
    purchase_dates = [
        # End date occurs before start date
        ('2020-02-02', '2020-02-01', 'MIT10',),
        ('2020-02-02', '2021-02-01', 'MIT10',),
        ('2021-02-25', '2023-02-03', 'Sustaining',),
    ]
    advance_id = '1234'
    username = 'foo'
    email = 'alice@example.com'
    first_name = 'Alice'
    last_name = 'Bob'
    for purchased_at, expires_at, level in purchase_dates:
        purchased_at = clean_datetime(purchased_at)
        expires_at = clean_datetime(expires_at)
        purchase = Purchase.create(
            commerce_instance_uid=str(uuid.uuid4()),
            advance_id=advance_id,
            first_name=first_name,
            last_name=last_name,
            email=email,
            level=level,
            purchased_at=purchased_at,
            purchase_price=50,
            scholarship_contribution=None,
            purchase_promo_code=None,
            expires_at=expires_at,
            is_lifetime=False,
            auto_renew_enabled=False,
            username=username,
        )
        upsert_membership_row(purchase)

    memberships = list(Membership.select().where(Membership.advance_id == advance_id).order_by(Membership.started_at))
    assert len(memberships) == 2

    membership = memberships[0]
    assert membership.started_at == 1580601600
    assert membership.ended_at == 1612137600
    assert membership.email == email
    assert membership.username == username
    assert membership.first_name == first_name
    assert membership.last_name == last_name
    assert membership.level == 'MIT10'

    membership = memberships[1]
    assert membership.started_at == 1614211200
    assert membership.ended_at == 1675382400
    assert membership.email == email
    assert membership.username == username
    assert membership.first_name == first_name
    assert membership.last_name == last_name
    assert membership.level == 'Sustaining'
