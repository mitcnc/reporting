import datetime
import decimal

import pytest
import pytz

from ..utils import (
    TIMEZONE, clean_boolean, clean_cybersource_transaction_id, clean_datetime, clean_decimal, clean_membership_level,
    clean_string, strip_time_from_timestamp
)


def test_clean_membership_level():
    data = {
        # Sustaining
        'MEM - CNC - Regular': 'Sustaining',
        'MEM - CNC - Regular (OLD)': 'Sustaining',
        'MEM - CNC - Sustaining': 'Sustaining',
        'MEM - CNC - Sustaining (OLD)': 'Sustaining',

        # MIT10
        'MEM - CNC - MIT10': 'MIT10',
        'MEM - CNC - Recent Grad (OLD)': 'MIT10',
        'MEM - CNC - Recent Grad': 'MIT10',
        'MEM - NC - Recent Grad (OLD)': 'MIT10',

        # Life
        'MEM - CNC - Life': 'Life',
        'MEM - CNC - Senior Alumnus': 'Life',
        'MEM - CNC - Life (Membership Terminiation)': 'Life',
        'MEM - CNC - Cardinal & Gray (OLD)': 'Life',
        'MEM - NC - Cardinal and Gray (NEW)': 'Life',
        'MEM - CNC - Life (OLD)': 'Life',
        'MEM - NC - Life (OLD)': 'Life',

        # Student
        'MEM - CNC - Student': 'Student',
        'MEM - CNC - Student (OLD)': 'Student',
        'MEM - NC - Student (OLD)': 'Student',

        # Patron
        'MEM - CNC - Patron': 'Patron',
        'MEM - CNC - Patron (OLD)': 'Patron',

        'MEM - CNC - Supporting': 'Supporting',
        'MEM - CNC - Benefactor': 'Benefactor',
    }

    for dirty, expected in data.items():
        assert clean_membership_level(dirty) == expected, f'Invalid output for {dirty}'


def test_clean_boolean():
    data = {
        'True': True,
        'TRUE': True,
        'true': True,
        '1': True,
        'False': False,
        'FALSE': False,
        'false': False,
        '0': False,
    }

    for dirty, expected in data.items():
        assert clean_boolean(dirty) == expected, f'Invalid output for {dirty}'


@pytest.mark.parametrize("test_input,expected", [
    ('1/12/2018 8:14:01 AM', datetime.datetime(2018, 1, 12, 8, 14, 1)),
    ('11/9/2018', datetime.datetime(2018, 11, 9)),
    ('2016-09-22 15:51:13-07:00', datetime.datetime(2016, 9, 22, 15, 51, 13)),
    ('2018-11-09 00:00:00-08:00', datetime.datetime(2018, 11, 9)),
    ('', None),
])
def test_clean_datetime(test_input, expected):
    if expected:
        expected = TIMEZONE.localize(expected)
        expected = expected.astimezone(pytz.utc)
        expected = int(expected.timestamp())
    assert clean_datetime(test_input) == expected


def test_strip_time_from_timestamp():
    assert strip_time_from_timestamp(1657001528) == \
           int(datetime.datetime(2022, 7, 5, 0, 0, 0, tzinfo=pytz.UTC).timestamp())


def test_clean_decimal():
    assert clean_decimal('') is None
    assert clean_decimal('100') == decimal.Decimal(100)


def test_clean_string():
    assert clean_string('') is None
    assert clean_string('ABC') == 'ABC'
    assert clean_string(' ABC ') == 'ABC'


def test_clean_cybersource_transaction_id():
    assert clean_cybersource_transaction_id('') is None
    assert clean_cybersource_transaction_id('Skipped billing - not applicable') is None
    assert clean_cybersource_transaction_id('ABC5514708510046927803093') is None
    assert clean_cybersource_transaction_id('5514708510046927803093') == '5514708510046927803093'
    assert clean_cybersource_transaction_id(' 5514708510046927803093 ') == '5514708510046927803093'
