import datetime
import io
import os
from decimal import Decimal
from typing import BinaryIO, Dict, List, Optional, Union, get_args, get_origin

from google.api_core.exceptions import BadRequest, Conflict
from google.cloud import bigquery

from helpers.stripe import Invoice
from reporting.eventbrite import Attendee
from reporting.stripe import Customer, Subscription
from reporting.wordpress import User

PROJECT_ID: str = 'mitcnc-membership'
DATASET_ID: str = 'reporting'
ATTENDEES_TABLE_ID: str = 'attendees'
EVENTS_TABLE_ID: str = 'events'
IMDOULES_MEMBERS_TABLE_ID: str = 'memberships_imodules'
IMODULES_MEMBERSHIP_PURCHASES_TABLE_ID: str = 'membership_purchases_imodules'
STRIPE_CUSTOMERS_TABLE_ID: str = 'stripe_customers'
STRIPE_INVOICES_TABLE_ID: str = 'stripe_invoices'
STRIPE_SUBSCRIPTIONS_TABLE_ID: str = 'stripe_subscriptions'
STRIPE_MEMBERSHIPS_TABLE_ID: str = 'memberships_stripe'
WORDPRESS_USERS_TABLE_ID: str = 'wordpress_users'
DECEASED_MEMBERS_TABLE_ID: str = 'deceased_members'


def is_optional(field) -> bool:
    return get_origin(field) is Union and type(None) in get_args(field)


def get_schema_type(field_type: type) -> str:
    raw_type = field_type
    is_array = False

    origin = get_origin(field_type)
    if origin:
        types = list(get_args(field_type))

        # Remove NoneType for Optional types
        if type(None) in types:
            types.remove(type(None))

            # NOTE: BigQuery only supports scalars and structs.
        #   We only deal with scalars.
        raw_type = types[0]

        is_array = origin == list

    scalar_type = {
        bool: 'BOOLEAN',
        int: 'INTEGER',
        str: 'STRING',
        Decimal: 'NUMERIC',
    }[raw_type]

    # NOTE: 'ARRAY<STRING>' is not supported. Instead, the mode
    #   will be set to 'REPEATED'.
    if is_array and raw_type != str:
        return f'ARRAY<{scalar_type}>'

    return scalar_type


def get_schema_mode(field_type: type) -> str:
    if is_optional(field_type):
        return 'NULLABLE'

    if get_origin(field_type) == list:
        return 'REPEATED'

    return 'REQUIRED'


def schema_from_dataclass(
        dataclass: type,
        type_overrides: Optional[Dict[str, str]] = None
) -> List[bigquery.SchemaField]:
    schema = []

    if type_overrides is None:
        type_overrides = {}

    for name, definition in dataclass.__dataclass_fields__.items():  # type: ignore
        _type = type_overrides.get(name)
        if _type is None:
            _type = get_schema_type(definition.type)

        mode = get_schema_mode(definition.type)

        schema.append(bigquery.SchemaField(name, _type, mode=mode))

    return schema


def is_connected() -> bool:
    client = bigquery.Client()

    try:
        client.get_service_account_email()
    except Exception:  # pylint: disable=broad-except
        return False

    return True


def _stringio2bytesio(data: io.StringIO) -> io.BytesIO:
    return io.BytesIO(data.getvalue().encode('utf8'))


def _recreate_table(client: bigquery.Client, table_name: str, schema: List[bigquery.SchemaField]) -> None:
    print(f'Recreating {table_name} table...')
    table_id = f'{PROJECT_ID}.{DATASET_ID}.{table_name}'
    table = bigquery.Table(table_id, schema=schema)
    client.delete_table(table, not_found_ok=True)
    client.create_table(table)


def _refresh_table_data(
        table_name: str,
        schema: List[bigquery.SchemaField],
        source_file: Union[io.BytesIO, io.IOBase, BinaryIO],
        source_format: str = bigquery.SourceFormat.CSV,
        recreate_table: bool = True
):
    client = bigquery.Client()

    if recreate_table:
        _recreate_table(client, table_name, schema)

    print(f'Loading {table_name} data into BigQuery...')

    job_config = bigquery.LoadJobConfig(source_format=source_format)

    if source_format == bigquery.SourceFormat.CSV:
        job_config.skip_leading_rows = 1

    table_id = f'{PROJECT_ID}.{DATASET_ID}.{table_name}'
    job = client.load_table_from_file(
        source_file,  # type: ignore
        table_id,
        location='US',
        job_config=job_config
    )

    # Wait for the loading to complete
    job.result()

    print(f'Loaded {job.output_rows} rows into {DATASET_ID}:{table_name}.')


def refresh_attendee_data(data: io.StringIO) -> None:
    source_file = _stringio2bytesio(data)
    overrides = {
        'created': 'TIMESTAMP',
        'updated': 'TIMESTAMP',
    }
    schema = schema_from_dataclass(Attendee, type_overrides=overrides)
    _refresh_table_data(
        ATTENDEES_TABLE_ID,
        schema,
        source_file,
        source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON,
        recreate_table=False
    )


def refresh_event_data(data: io.StringIO) -> None:
    source_file = _stringio2bytesio(data)
    schema = [
        bigquery.SchemaField('id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('name', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('start', 'TIMESTAMP', mode='NULLABLE'),
        bigquery.SchemaField('end', 'TIMESTAMP', mode='NULLABLE'),
        bigquery.SchemaField('status', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('url', 'STRING'),
        bigquery.SchemaField('venue_id', 'STRING', mode='NULLABLE'),
        bigquery.SchemaField('category', 'STRING', mode='NULLABLE'),
        bigquery.SchemaField('subcategory', 'STRING', mode='NULLABLE'),
    ]
    _refresh_table_data(EVENTS_TABLE_ID, schema, source_file)


def refresh_stripe_customer_data(data: io.StringIO) -> None:
    source_file = _stringio2bytesio(data)
    schema = schema_from_dataclass(Customer)
    _refresh_table_data(
        STRIPE_CUSTOMERS_TABLE_ID,
        schema,
        source_file,
        source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    )


def refresh_stripe_invoice_data(data: io.StringIO) -> None:
    source_file = _stringio2bytesio(data)
    schema = schema_from_dataclass(Invoice)
    _refresh_table_data(
        STRIPE_INVOICES_TABLE_ID,
        schema,
        source_file,
        source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    )


def refresh_stripe_subscription_data(data: io.StringIO) -> None:
    source_file = _stringio2bytesio(data)
    overrides = {
        'created': 'TIMESTAMP',
        'canceled_at': 'TIMESTAMP',
        'current_period_start': 'TIMESTAMP',
        'current_period_end': 'TIMESTAMP',
        'ended_at': 'TIMESTAMP',
        'start_date': 'TIMESTAMP',
        'trial_start': 'TIMESTAMP',
        'trial_end': 'TIMESTAMP',
        'amount': 'NUMERIC',
    }
    schema = schema_from_dataclass(Subscription, type_overrides=overrides)
    _refresh_table_data(
        STRIPE_SUBSCRIPTIONS_TABLE_ID,
        schema,
        source_file,
        source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    )


def refresh_wordpress_user_data(source_file: io.IOBase) -> None:
    overrides = {
        'last_login': 'TIMESTAMP',
    }
    schema = schema_from_dataclass(User, type_overrides=overrides)
    _refresh_table_data(
        WORDPRESS_USERS_TABLE_ID,
        schema,
        source_file,
        source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON
    )


def refresh_imodules_membership_data(source_file: io.IOBase) -> None:
    schema = [
        bigquery.SchemaField('advance_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('username', 'STRING', mode='NULLABLE'),
        bigquery.SchemaField('first_name', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('last_name', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('email', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('level', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('started_at', 'TIMESTAMP', mode='REQUIRED'),
        bigquery.SchemaField('ended_at', 'TIMESTAMP', mode='NULLABLE'),
        bigquery.SchemaField('cybersource_transaction_ids', 'STRING', mode='NULLABLE'),
    ]
    _refresh_table_data(IMDOULES_MEMBERS_TABLE_ID, schema, source_file)


def refresh_membership_purchase_imodules_data(source_file: io.IOBase) -> None:
    schema = [
        bigquery.SchemaField('commerce_instance_uid', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('advance_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('username', 'STRING', mode='NULLABLE'),
        bigquery.SchemaField('first_name', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('last_name', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('email', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('level', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('purchased_at', 'TIMESTAMP'),
        bigquery.SchemaField('purchase_price', 'INTEGER'),
        bigquery.SchemaField('scholarship_contribution', 'INTEGER'),
        bigquery.SchemaField('purchase_promo_code', 'STRING', mode='NULLABLE'),
        bigquery.SchemaField('expires_at', 'TIMESTAMP', mode='NULLABLE'),
        bigquery.SchemaField('is_lifetime', 'BOOLEAN', mode='REQUIRED'),
        bigquery.SchemaField('auto_renew_enabled', 'BOOLEAN', mode='REQUIRED'),
        bigquery.SchemaField('cybersource_transaction_id', 'STRING', mode='NULLABLE'),
    ]
    _refresh_table_data(IMODULES_MEMBERSHIP_PURCHASES_TABLE_ID, schema, source_file)


def refresh_deceased_member_data() -> None:
    """ Load identifiers for deceased members.

    This is an ad hoc function that is not run with any automated imports, but
    should be run locally as data is updated.
    """

    schema = [
        bigquery.SchemaField('advance_id', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('date', 'DATE', mode='NULLABLE'),
    ]
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, 'data/deceased.csv')
    with open(filename, 'rb') as source_file:
        _refresh_table_data(DECEASED_MEMBERS_TABLE_ID, schema, source_file)


def _create_view(table_id: str, query: str, description: str):
    client = bigquery.Client()

    view = bigquery.Table(table_id)
    view.view_query = query
    view.description = description

    try:
        view = client.create_table(view)
        print("Successfully created view at {}".format(view.full_table_id))
    except Conflict:
        view = client.update_table(view, ['description', 'view_query'])
        print("Successfully updated view at {}".format(view.full_table_id))


def create_memberships_view():
    table_id = f'{PROJECT_ID}.{DATASET_ID}.memberships'
    description = 'Memberships for all time, merged from Encompass and Stripe data'

    query = """
    WITH
      min_start_dates AS (
      SELECT
        advance_id,
        MIN(start_date) AS min_start_date
      FROM
        `reporting.memberships_stripe`
      GROUP BY
        1)
    SELECT
      i.advance_id,
      i.username,
      i.first_name,
      i.last_name,
      i.email,
      i.level,
      i.started_at AS purchased_at,
    IF
      (i.level = 'Life',
        NULL,
        COALESCE(LEAST(i.ended_at, d.min_start_date),
          i.ended_at,
          TIMESTAMP('2020-11-05'))) AS expires_at,
      NULL AS stripe_subscription_id,
      i.cybersource_transaction_ids AS cybersource_transaction_ids,
      'imodules' AS source
    FROM
      `reporting.memberships_imodules` i
    LEFT JOIN
      `reporting.memberships_stripe` s
    ON
      s.cybersource_transaction_id IN UNNEST(SPLIT(cybersource_transaction_ids, ","))
    LEFT JOIN
      min_start_dates d
    ON
      i.advance_id = d.advance_id
    WHERE
      s.id IS NULL
      AND (d.min_start_date IS NULL
        OR i.started_at < d.min_start_date)
    UNION ALL
      -- Add Stripe purchases
      (
      SELECT
        s.advance_id,
        s.username,
        s.first_name,
        s.last_name,
        s.email,
        s.level,
        COALESCE(LEAST(s.start_date, i.started_at),
          s.start_date) AS purchased_at,
        COALESCE(s.ended_at,
          CASE
            WHEN cancel_at_period_end THEN current_period_end
          ELSE
          NULL
        END
          ) AS expires_at,
        s.id AS stripe_subscription_id,
        s.cybersource_transaction_id AS cybersource_transaction_ids,
        'stripe' AS source
      FROM
        `mitcnc-membership.reporting.memberships_stripe` s
      LEFT JOIN
        `reporting.memberships_imodules` i
      ON
        s.cybersource_transaction_id IN UNNEST(SPLIT(i.cybersource_transaction_ids, ",")) )
    ORDER BY
      purchased_at,
      advance_id
    """

    _create_view(table_id, query, description)


def create_stripe_memberships_view():
    table_id = f'{PROJECT_ID}.{DATASET_ID}.{STRIPE_MEMBERSHIPS_TABLE_ID}'
    description = 'Memberships recorded at Stripe'

    query = """
    WITH
      encompass_start_dates AS (
      SELECT
        advance_id,
        MAX(started_at) AS started_at
      FROM
        `reporting.memberships_imodules`
      GROUP BY
        1 ),
      missing_transaction_ids AS (
      SELECT
        s.id,
        pi.cybersource_transaction_id
      FROM
        `reporting.stripe_subscriptions` s
      JOIN
        reporting.stripe_customers c
      ON
        s.customer = c.id
      JOIN
        reporting.wordpress_users u
      ON
        u.stripe_customer_id = c.id
      JOIN
        `reporting.memberships_imodules` i
      ON
        u.advance_id = i.advance_id
        AND REGEXP_REPLACE(s.price_name, r'\s+Membership$', '') = i.level
        AND i.cybersource_transaction_ids IS NOT NULL
        AND i.cybersource_transaction_ids <> ''
      JOIN
        `reporting.membership_purchases_imodules` pi
      ON
        pi.advance_id = i.advance_id
        AND pi.cybersource_transaction_id IN UNNEST(SPLIT(i.cybersource_transaction_ids, ","))
        AND pi.expires_at >= s.ended_at
      JOIN
        encompass_start_dates sd
      ON
        sd.advance_id = i.advance_id
        AND sd.started_at = i.started_at
      WHERE
        note LIKE 'Non-renewing migration%' )
    SELECT
      u.advance_id,
      u.username,
      u.first_name,
      u.last_name,
      c.email,
      REGEXP_REPLACE(s.price_name, r'\s+Membership$', '') AS level,
      s.amount,
      s.current_period_start,
      s.current_period_end,
      s.cancel_at_period_end,
      s.canceled_at,
      s.start_date,
      s.ended_at,
      s.id,
      COALESCE(s.cybersource_transaction_id,
        mti.cybersource_transaction_id) AS cybersource_transaction_id,
      s.note,
      s.trial_start,
      s.trial_end,
      s.latest_invoice
    FROM
      reporting.stripe_subscriptions s
    LEFT JOIN
      missing_transaction_ids mti
    ON
      s.id = mti.id
    JOIN
      reporting.stripe_customers c
    ON
      s.customer = c.id
    LEFT JOIN
      reporting.wordpress_users u
    ON
      u.stripe_customer_id = c.id
    ORDER BY
      advance_id
    """

    _create_view(table_id, query, description)


def create_memberships_export_view():
    create_routines()

    table_id = f'{PROJECT_ID}.{DATASET_ID}.encompass_memberships_export'
    description = 'Memberships formatted for export to Encompass'

    # NOTE (clintonb): `last_purchased_at` is actually the membership start date.
    #   We are opting not to rename since the script reading this data is already
    #   running elsewhere.
    # NOTE: In reality expires_at should be NULL for Life and Cardinal & Gray memberships. However,
    #   MITAA systems have Cardinal & Gray expiring after a year, and this cannot be changed. We
    #   work around this by treating Cardinal & Gray/Senior Alumnus as "Life".
    query = """
    SELECT
      m.advance_id,
      CONCAT('MEM - CNC - ',
      IF
        (m.level = 'Cardinal & Gray', 'Life',
        IF
          (m.level = 'Regular', 'Regular (OLD)', m.level))) AS level,
      m.start_date AS last_purchased_at,
    IF
      (level = 'Cardinal & Gray', NULL, COALESCE(canceled_at, m.current_period_end)) AS expires_at
    FROM
      `reporting.memberships_stripe` m
    JOIN
      `reporting.stripe_invoices` si
    ON
      si.id = m.latest_invoice
    WHERE
      (m.ended_at IS NULL
        OR m.ended_at > CURRENT_TIMESTAMP())
      AND si.paid
    ORDER BY
      m.current_period_end DESC,
      m.advance_id
        """

    _create_view(table_id, query, description)


def get_eventbrite_attendee_changed_since() -> Optional[datetime.datetime]:
    table_id = f'{PROJECT_ID}.{DATASET_ID}.attendees'

    client = bigquery.Client()
    query = """
        SELECT
          MAX(updated) AS last_changed
        FROM
          `{}`
    """.format(table_id)
    query_job = client.query(query)

    try:
        results = query_job.result()
        # NOTE: We add one second to avoid pulling the record with the maximum last_changed value.
        return list(results)[0]['last_changed'] + datetime.timedelta(seconds=1)
    except BadRequest as e:
        print(f'Failed to get Eventbrite last updated date.\n{e}')
        return None


def create_attendees_annotated_view():
    table_id = f'{PROJECT_ID}.{DATASET_ID}.attendees_annotated'
    description = 'Attendees with annotated data (e.g., advance_id)'

    attendees_table_id = f'{PROJECT_ID}.{DATASET_ID}.{ATTENDEES_TABLE_ID}'
    users_table_id = f'{PROJECT_ID}.{DATASET_ID}.{WORDPRESS_USERS_TABLE_ID}'
    query = """
    SELECT
      a.*,
      u.advance_id
    FROM
      `{0}` a
    CROSS JOIN
      `{1}` u
    WHERE
      (LOWER(a.username) = LOWER(u.username)
        OR LOWER(a.email) = LOWER(u.email))
    """.format(attendees_table_id, users_table_id)

    _create_view(table_id, query, description)


def _update_or_create_routine(routine: bigquery.Routine):
    client = bigquery.Client()

    try:
        print(f"Attempting to create routine {routine.routine_id}")
        result = client.create_routine(routine)
        print(f"Successfully created routine {result.reference}")
    except Conflict:
        client.delete_routine(routine)
        result = client.create_routine(routine)
        print(f'Successfully updated routine {result.reference}')


def create_routines():
    id_prefix = f'{PROJECT_ID}.{DATASET_ID}'
    routine = bigquery.Routine(
        f'{id_prefix}.level_to_int',
        type_="SCALAR_FUNCTION",
        language="SQL",
        description="Converts membership level to integer. Ordering is suitable for comparing and de-duplicating "
                    "for a member with two memberships.",
        body="""
        CASE level
          WHEN 'Life' THEN 0
          WHEN 'Cardinal & Gray' THEN 1
          WHEN 'Sponsor' THEN 2
          WHEN 'Partner' THEN 3
          WHEN 'Benefactor' THEN 4
          WHEN 'Supporting' THEN 5
          WHEN 'Patron' THEN 6
          WHEN 'Sustaining' THEN 7
          WHEN 'MIT10' THEN 8
          WHEN 'Student' THEN 9
          ELSE
            1000
        END
        """,
        # return_type=bigquery.StandardSqlTypeNames.INT64,
        arguments=[
            bigquery.RoutineArgument(
                name="level",
                data_type=bigquery.StandardSqlDataType(
                    type_kind=bigquery.StandardSqlTypeNames.STRING
                ),
            )
        ],
    )
    _update_or_create_routine(routine)

    routine = bigquery.Routine(
        f'{id_prefix}.int_to_level',
        type_="SCALAR_FUNCTION",
        language="SQL",
        description="Converts integer to membership level",
        body="""
        CASE level
          WHEN 0 THEN 'Life'
          WHEN 1 THEN 'Cardinal & Gray'
          WHEN 2 THEN 'Sponsor'
          WHEN 3 THEN 'Partner'
          WHEN 4 THEN 'Benefactor'
          WHEN 5 THEN 'Supporting'
          WHEN 6 THEN 'Patron'
          WHEN 7 THEN 'Sustaining'
          WHEN 8 THEN 'MIT10'
          WHEN 9 THEN 'Student'
          ELSE
            NULL
        END
        """,
        # return_type=bigquery.StandardSqlTypeNames.STRING,
        arguments=[
            bigquery.RoutineArgument(
                name="level",
                data_type=bigquery.StandardSqlDataType(
                    type_kind=bigquery.StandardSqlTypeNames.INT64
                ),
            )
        ],
    )
    _update_or_create_routine(routine)
