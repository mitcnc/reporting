import csv
import datetime
import io
from dataclasses import dataclass
from decimal import Decimal
from typing import Dict, List, Optional, Union, cast

import pytz
from eventbrite import Eventbrite

from reporting.utils import clean_datetime, clean_decimal

NESTING_SEPARATOR = '__'
PAGE_SIZE = 1000


@dataclass
class Attendee:
    id: str
    created: int
    updated: int
    first_name: Optional[str] = None
    last_name: Optional[str] = None
    username: Optional[str] = None
    email: Optional[str] = None
    event_id: Optional[str] = None
    ticket_display_price: Optional[Decimal] = None
    ticket_revenue: Optional[Decimal] = None


class EventbriteWrapper:
    def __init__(self, api_key: str):
        self.client = Eventbrite(api_key)

    def get_user(self) -> dict:
        return self.client.get_user()

    def is_connected(self) -> bool:
        return bool(self.client.get_user())

    def get_events(self) -> List[dict]:
        events: List[dict] = []
        client = self.client
        user = self.get_user()

        continuation = None
        while True:
            # NOTE: We have to call the endpoint manually because Eventbrite
            #   deprecated the old endpoint, but did not update the API client.
            #   See https://github.com/eventbrite/eventbrite-sdk-python/issues/54.
            user_id = user['id']
            response = client.get(
                f'/organizations/{user_id}/events/',
                data={
                    'continuation': continuation,
                    'expand': 'category,subcategory',
                    'page_size': PAGE_SIZE,
                }
            )
            events += response['events']
            page_number = response["pagination"]["page_number"]
            page_count = response["pagination"].get("page_count")
            print(f'Retrieved events page {page_number} of {page_count}')

            continuation = response['pagination'].get('continuation')
            if continuation is None:
                break

        return events

    def _get_field_value(self, item: dict, field: str) -> Optional[Union[int, str]]:
        if not item:
            return None

        if NESTING_SEPARATOR in field:
            first, rest = field.split(NESTING_SEPARATOR, 1)
            return self._get_field_value(item.get(first, {}), rest)

        return item.get(field)

    def get_events_csv(self, events: List[dict]) -> io.StringIO:
        keys = ['id', 'name', 'start', 'end', 'status', 'url', 'venue_id', 'category', 'subcategory', ]
        aliases = {
            'name': 'name__text',
            'start': 'start__utc',
            'end': 'end__utc',
            'category': 'category__name',
            'subcategory': 'subcategory__name',
        }

        buffer = io.StringIO()
        writer = csv.DictWriter(buffer, fieldnames=keys, quoting=csv.QUOTE_ALL)
        writer.writeheader()

        for event in events:
            d = {key: self._get_field_value(event, aliases.get(key, key)) for key in keys}
            writer.writerow(d)

        buffer.seek(0)
        return buffer

    def get_attendees(self, changed_since: Optional[datetime.datetime]) -> List[Attendee]:
        attendees: List[Attendee] = []
        client = self.client
        user = self.get_user()

        continuation = None
        while True:
            # NOTE (clintonb): As of 2020-10-06, this endpoint does not support page_size.
            #   https://www.eventbrite.com/platform/api#/reference/attendee/list/list-attendees-by-organization
            user_id = user['id']
            data: Dict[str, Optional[str]] = {
                'continuation': continuation,
            }
            if changed_since:
                data['changed_since'] = changed_since.astimezone(pytz.UTC).strftime('%Y-%m-%dT%H:%M:%SZ')

            response = client.get(f'/organizations/{user_id}/attendees/', data=data)
            parsed_attendees = self._parse_attendees(response['attendees'])
            attendees += parsed_attendees

            page_number = response['pagination']['page_number']
            page_count = response['pagination'].get('page_count')
            print(f'Retrieved attendees page {page_number} of {page_count}')

            continuation = response['pagination'].get('continuation')
            if continuation is None:
                break

        return attendees

    def _get_attendee_username(self, attendee: dict) -> Optional[str]:
        answers = attendee['answers']
        for answer in answers:
            if 'alum.mit.edu' in answer['question']:
                username = answer.get('answer', '').strip().lower()

                # Some people may only provide the username, without the domain.
                # Others may provide a completely different email address. Filter these
                # out, along with empty strings.
                if username and ('@' not in username or '@alum' in username):
                    username = username.split('@')[0]
                    return username

        return None

    def _parse_attendees(self, attendees: List[dict]) -> List[Attendee]:
        keys = list(Attendee.__dataclass_fields__.keys())  # type: ignore
        aliases = {
            'updated': 'changed',
            'first_name': 'profile__first_name',
            'last_name': 'profile__last_name',
            'email': 'profile__email',
            'ticket_revenue': 'costs__base_price__major_value',
            'ticket_display_price': 'costs__gross__major_value',
        }

        parsed = []
        for attendee in attendees:
            d = {
                key: self._get_field_value(attendee, aliases.get(key, key)) for key in keys}

            parsed.append(Attendee(
                id=cast(str, d['id']),
                created=cast(int, clean_datetime(cast(str, d['created']))),
                updated=cast(int, clean_datetime(cast(str, d['updated']))),
                first_name=cast(Optional[str], d['first_name']),
                last_name=cast(Optional[str], d['last_name']),
                username=self._get_attendee_username(attendee),
                email=cast(Optional[str], d['email']),
                event_id=cast(Optional[str], d['event_id']),
                ticket_display_price=clean_decimal(cast(str, d['ticket_display_price'])),
                ticket_revenue=clean_decimal(cast(str, d['ticket_revenue'])),
            ))

        return parsed
