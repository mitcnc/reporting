from typing import List

import stripe

from helpers.stripe import Customer, Invoice, Subscription


class StripeImporter:
    def __init__(self, api_key: str):
        stripe.api_key = api_key

    def get_customers(self) -> List[Customer]:
        customers = []
        api_customers = stripe.Customer.list(limit=100)

        for customer in api_customers.auto_paging_iter():
            wordpress_user_id = customer['metadata'].get('wordpress_user_id')
            wordpress_user_id = int(wordpress_user_id) if wordpress_user_id else None

            email = customer['email']

            if email:
                email = email.lower()

            customers.append(Customer(
                id=customer['id'],
                email=email,
                name=customer['name'],
                wordpress_user_id=wordpress_user_id
            ))

        print(f'Retrieved {len(customers)} customers from Stripe')
        return customers

    def get_invoices(self) -> List[Invoice]:
        converted_invoices = []

        api_invoices = stripe.Invoice.list(
            limit=100,
        )
        for api_invoice in api_invoices.auto_paging_iter():
            converted_invoices.append(Invoice.from_api_object(api_invoice))

        print(f'Retrieved {len(converted_invoices)} invoices from Stripe')
        return converted_invoices

    def get_subscriptions(self) -> List[Subscription]:
        subscriptions = []

        api_subscriptions = stripe.Subscription.list(
            limit=100,
            expand=['data.plan.product'],
            status='all',
        )
        for api_subscription in api_subscriptions.auto_paging_iter():
            subscriptions.append(Subscription.from_api_object(api_subscription))

        print(f'Retrieved {len(subscriptions)} subscriptions from Stripe')
        return subscriptions


def cancel_subscription(api_key: str, subscription_id: str):
    stripe.api_key = api_key
    stripe.Subscription.delete(subscription_id)
