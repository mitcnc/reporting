from dataclasses import dataclass
from datetime import datetime
from typing import List

import stripe
from google.cloud import bigquery

from helpers.stripe import Subscription


@dataclass
class Membership:
    class_year: int
    subscription: Subscription


class MIT10Scheduler:
    def __init__(self, stripe_api_key: str, mit10_price_id: str, regular_price_id: str):
        stripe.api_key = stripe_api_key

        self.mit10_price_id = mit10_price_id
        self.regular_price_id = regular_price_id

    def process(self):
        memberships = self._get_memberships()
        print(f"Retrieved {len(memberships)} subscriptions to update with MIT10 schedule")

        for membership in memberships:
            if self._should_update(membership):
                self._create_schedule(membership)

    def _get_memberships(self) -> List[Membership]:
        memberships = []

        client = bigquery.Client()
        query = """
        SELECT
          u.class_year,
          s.*
        FROM
          reporting.stripe_subscriptions s
        JOIN
          reporting.wordpress_users u
        ON
          s.customer = u.stripe_customer_id
        WHERE
          s.price_id = '{}'
          AND s.schedule IS NULL
          AND u.class_year IS NOT NULL
        """.format(self.mit10_price_id)
        query_job = client.query(query)

        for db_row in query_job:
            class_year = db_row.class_year
            dict_row = {k: v for (k, v) in db_row.items()}
            dict_row.pop('class_year')
            subscription = Subscription(**dict_row)
            memberships.append(Membership(class_year=class_year, subscription=subscription))

        return memberships

    def _should_update(self, membership: Membership) -> bool:
        """
        Returns a boolean indicating if the subscription needs to be updated
            with an MIT10 schedule.

        Args:
            membership (Membership)

        Returns:
            bool
        """
        subscription = membership.subscription
        return subscription.schedule is None and \
            subscription.ended_at is None and \
            subscription.price_id == self.mit10_price_id

    def _create_schedule(self, membership: Membership) -> None:
        subscription = membership.subscription

        # Pull the latest data from API in case we haven't yet updated BigQuery
        api_subscription = stripe.Subscription.retrieve(subscription.id, expand=['plan.product'])
        subscription = Subscription.from_api_object(api_subscription)
        membership.subscription = subscription

        if not self._should_update(membership):
            print(f"No need to update MIT10 subscription {subscription.id}")
            return

        try:
            # Create a subscription schedule based on the existing subscription
            schedule = stripe.SubscriptionSchedule.create(from_subscription=subscription.id)

            # Determine the number of years to allow MIT10 pricing.
            # Default to 1 year so that existing memberships transition at the end
            # of the current billing period.
            iterations = membership.class_year + 10 - datetime.now().year
            if iterations < 1:
                iterations = 1

            # Update the schedule with the correct phases
            schedule = stripe.SubscriptionSchedule.modify(
                schedule['id'],
                end_behavior='release',
                phases=[
                    {
                        'items': [
                            {
                                'price': self.mit10_price_id,
                            },
                        ],
                        'iterations': iterations,
                        'start_date': schedule['phases'][0]['start_date'],
                    },
                    {
                        'items': [
                            {
                                'price': self.regular_price_id,
                            },
                        ],
                    }
                ]
            )

            print(f"Added schedule to MIT10 subscription {subscription.id}: {schedule['id']}")
        except Exception as e:
            print(f"Failed to add schedule to MIT10 subscription {subscription.id}: {e}")
