import logging
from typing import List

import stripe
from google.cloud import bigquery


class PauseCollectionForLifetimeSubscriptions:
    """
    This class/script is responsible for pausing Stripe collection for lifetime memberships.
    Pausing collection disables reminder emails that confuse members, and lead to unnecessary
    support emails/calls.
    """

    def __init__(self, stripe_api_key: str, cardinal_and_gray_price_id: str):
        stripe.api_key = stripe_api_key

        self.cardinal_and_gray_price_id = cardinal_and_gray_price_id

    def process(self):
        subscription_ids = self._get_subscription_ids()
        print(f"Retrieved {len(subscription_ids)} subscriptions to pause collection from BigQuery")

        for subscription_id in subscription_ids:
            self._pause_collection(subscription_id)

    def _get_subscription_ids(self) -> List[str]:
        subscription_ids = []

        client = bigquery.Client()
        query = """
                SELECT
                  s.id AS subscription_id
                FROM
                  reporting.stripe_subscriptions s
                WHERE
                  s.price_id = '{}'
                  AND s.pause_collection__behavior IS NULL
                  AND s.ended_at IS NULL
                """.format(self.cardinal_and_gray_price_id)
        query_job = client.query(query)

        for db_row in query_job:
            subscription_ids.append(db_row.subscription_id)

        return subscription_ids

    def _pause_collection(self, subscription_id: str) -> None:
        try:
            stripe.Subscription.modify(subscription_id, pause_collection={"behavior": "mark_uncollectible"})
            print(f"Paused collection on subscription {subscription_id}")
        except Exception as e:
            print(f"Failed to pause collection on subscription {subscription_id}: {e}")
