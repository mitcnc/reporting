locals {
  project                = "mitcnc-membership"
  region                 = "us-central1"
  service_account_email  = google_service_account.data_importer.email
  source_repo_commit_sha = "eaa197441402b7c4e56af42e6c41ffbb8bde7ea3"
  source_repo_name       = google_sourcerepo_repository.reporting.name
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.43.1"
    }
  }

  backend "gcs" {
    bucket = "mitcnc-terraform-state"
    prefix = "terraform/state"
  }
}

provider "google" {
  project = local.project
  region  = local.region
}

resource "google_service_account" "data_importer" {
  account_id   = "data-importer-managed"
  display_name = "Data Importer"
}

resource "google_project_iam_custom_role" "data_importer" {
  role_id = "data_importer"
  title   = "Data Importer"
  permissions = [
    "bigquery.jobs.create",
    "bigquery.routines.create",
    "bigquery.routines.delete",
    "bigquery.routines.get",
    "bigquery.routines.list",
    "bigquery.routines.update",
    "bigquery.routines.updateTag",
    "bigquery.tables.create",
    "bigquery.tables.delete",
    "bigquery.tables.get",
    "bigquery.tables.getData",
    "bigquery.tables.list",
    "bigquery.tables.update",
    "bigquery.tables.updateData",
    "bigquery.tables.updateTag",
    "secretmanager.versions.access",
    "storage.buckets.get",
    "storage.buckets.list",
    "storage.objects.create",
    "storage.objects.get",
    "storage.objects.list",
  ]
}

resource "google_project_iam_member" "service_account_role" {
  project = local.project
  role    = google_project_iam_custom_role.data_importer.name
  member  = "serviceAccount:${local.service_account_email}"
}

resource "google_sourcerepo_repository" "reporting" {
  name = "reporting"
}

# Deploy functions to import Stripe data, and run every 6 hours
# NOTE: We max the timeout to 9 minutes since our Stripe data may grow over time
module "import_stripe_customers" {
  source                 = "./modules/scheduled_cloud_function"
  available_memory_mb    = 384
  entry_point            = "import_stripe_customers"
  name                   = "import-stripe-customers"
  runtime                = "python312"
  schedule               = "0 */6 * * *"
  service_account_email  = local.service_account_email
  timeout                = 540
  source_repo_name       = local.source_repo_name
  source_repo_commit_sha = local.source_repo_commit_sha
}

module "import_stripe_subscriptions" {
  source                 = "./modules/scheduled_cloud_function"
  available_memory_mb    = 384
  entry_point            = "import_stripe_subscriptions"
  name                   = "import-stripe-subscriptions"
  runtime                = "python312"
  schedule               = "0 */6 * * *"
  service_account_email  = local.service_account_email
  timeout                = 540
  source_repo_name       = local.source_repo_name
  source_repo_commit_sha = local.source_repo_commit_sha
}

module "import_stripe_invoices" {
  source                 = "./modules/scheduled_cloud_function"
  available_memory_mb    = 384
  entry_point            = "import_stripe_invoices"
  name                   = "import-stripe-invoices"
  runtime                = "python312"
  schedule               = "0 */6 * * *"
  service_account_email  = local.service_account_email
  timeout                = 540
  source_repo_name       = local.source_repo_name
  source_repo_commit_sha = local.source_repo_commit_sha
}

# This is intentionally run after the import to ensure this function operates on the latest data.
# This should (eventually) be updated to trigger based on an event published from the import function.
module "post_process_subscriptions" {
  source                 = "./modules/scheduled_cloud_function"
  available_memory_mb    = 256
  entry_point            = "post_process_subscriptions"
  name                   = "post-process-subscriptions"
  runtime                = "python312"
  schedule               = "15 */6 * * *"
  service_account_email  = local.service_account_email
  timeout                = 180
  source_repo_name       = local.source_repo_name
  source_repo_commit_sha = local.source_repo_commit_sha
  environment_variables = {
    CARDINAL_AND_GRAY_PRICE_ID : "price_1HQn2RCAwi9Ti4sNSY2rR8xs"
    MIT10_PRICE_ID : "price_1I1HAsCAwi9Ti4sN54NofB2I"
    REGULAR_PRICE_ID : "price_1HQOrgCAwi9Ti4sN3yrAIBCB"
  }
}

# Deploy function to refresh Eventbrite data, and run every 12 hours
# NOTE: We max the timeout to 9 minutes since loading all attendees takes a lot of time
module "refresh_eventbrite_events" {
  source                 = "./modules/scheduled_cloud_function"
  available_memory_mb    = 256
  entry_point            = "refresh_eventbrite_events"
  name                   = "refresh-eventbrite-events"
  runtime                = "python312"
  schedule               = "0 */12 * * *"
  service_account_email  = local.service_account_email
  timeout                = 540
  source_repo_name       = local.source_repo_name
  source_repo_commit_sha = local.source_repo_commit_sha
}

module "refresh_eventbrite_attendees" {
  source                 = "./modules/scheduled_cloud_function"
  available_memory_mb    = 256
  entry_point            = "refresh_eventbrite_attendees"
  name                   = "refresh-eventbrite-attendees"
  runtime                = "python312"
  schedule               = "0 */12 * * *"
  service_account_email  = local.service_account_email
  timeout                = 540
  source_repo_name       = local.source_repo_name
  source_repo_commit_sha = local.source_repo_commit_sha
}

# Refresh WordPress data every 3 hours
module "load_wordpress_users" {
  source                 = "./modules/scheduled_cloud_function"
  available_memory_mb    = 384
  entry_point            = "load_wordpress_users"
  name                   = "load-wordpress-users"
  runtime                = "python312"
  schedule               = "0 */3 * * *"
  service_account_email  = local.service_account_email
  timeout                = 300
  source_repo_name       = local.source_repo_name
  source_repo_commit_sha = local.source_repo_commit_sha
}
