resource "google_storage_bucket" "encompass_exports" {
  name                        = "mitcnc-membership-purchases"
  location                    = local.region
  uniform_bucket_level_access = true

  lifecycle_rule {
    condition {
      age = 31
    }
    action {
      type = "Delete"
    }
  }
}
locals {
  default_environment_variables = {
    GCP_PROJECT : local.project
  }
}

resource "google_cloudfunctions2_function" "download_encompass_data_export" {
  name        = "download-encompass-data-export"
  description = "Downloads membership exports from Encompass. Stripe is our system of record, so this should only be run on as needed."
  location    = local.region

  build_config {
    runtime               = "nodejs22"
    entry_point           = "downloadMembershipData"
    environment_variables = local.default_environment_variables
    source {
      repo_source {
        repo_name  = local.source_repo_name
        commit_sha = local.source_repo_commit_sha
      }
    }
  }

  service_config {
    max_instance_count    = 1
    available_memory      = "1024M"
    timeout_seconds       = 300
    ingress_settings      = "ALLOW_INTERNAL_ONLY"
    service_account_email = local.service_account_email
  }
}


resource "google_cloudfunctions2_function" "load_encompass_data" {
  name        = "load-encompass-data"
  description = "Transforms and loads Encompass data into the data warehouse. This is triggered when a new data export is downloaded to the Storage bucket."
  location    = local.region

  build_config {
    runtime               = "python312"
    entry_point           = "load_membership_data"
    environment_variables = local.default_environment_variables
    source {
      repo_source {
        repo_name  = local.source_repo_name
        commit_sha = local.source_repo_commit_sha
      }
    }
  }

  service_config {
    max_instance_count    = 1
    available_memory      = "512M"
    timeout_seconds       = 300
    ingress_settings      = "ALLOW_INTERNAL_ONLY"
    service_account_email = local.service_account_email
  }

  event_trigger {
    event_type     = "google.cloud.storage.object.v1.finalized"
    retry_policy   = "RETRY_POLICY_DO_NOT_RETRY"
    trigger_region = local.region
    event_filters {
      attribute = "bucket"
      value     = google_storage_bucket.encompass_exports.name
    }
  }
}
