variable "project" {
  type    = string
  default = "mitcnc-membership"
}

variable "region" {
  type    = string
  default = "us-central1"
}

variable "source_repo_name" {
  type = string
}

variable "source_repo_commit_sha" {
  type = string
}

variable "name" {
  type = string
}

variable "entry_point" {
  type = string
}

variable "service_account_email" {
  type = string
}

variable "environment_variables" {
  type    = map(string)
  default = {}
}

variable "available_memory_mb" {
  type    = number
  default = 128
}

variable "timeout" {
  type    = number
  default = 300
}

variable "runtime" {
  type = string
}

variable "schedule" {
  type = string
}