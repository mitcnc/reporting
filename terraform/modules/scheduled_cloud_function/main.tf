locals {
  default_environment_variables = {
    GCP_PROJECT : var.project
  }
}
# create pub/sub topic
resource "google_pubsub_topic" "topic" {
  name = var.name
}

# create a schedular (cron job) for every minute
resource "google_cloud_scheduler_job" "job" {
  name     = var.name
  schedule = var.schedule

  pubsub_target {
    topic_name = google_pubsub_topic.topic.id
    data       = base64encode("IGNORE-ME")
  }
}

resource "google_cloudfunctions2_function" "function" {
  name     = var.name
  location = var.region

  build_config {
    runtime               = var.runtime
    entry_point           = var.entry_point
    environment_variables = merge(local.default_environment_variables, var.environment_variables)
    source {
      repo_source {
        repo_name  = var.source_repo_name
        commit_sha = var.source_repo_commit_sha
      }
    }
  }

  service_config {
    max_instance_count    = 1
    available_memory      = "${var.available_memory_mb}M"
    timeout_seconds       = var.timeout
    ingress_settings      = "ALLOW_INTERNAL_ONLY"
    service_account_email = var.service_account_email
    environment_variables = merge(local.default_environment_variables, var.environment_variables)
  }

  event_trigger {
    event_type     = "google.cloud.pubsub.topic.v1.messagePublished"
    pubsub_topic   = google_pubsub_topic.topic.id
    retry_policy   = "RETRY_POLICY_DO_NOT_RETRY"
    trigger_region = var.region
  }
}
