const memberships = require('./memberships');
const {SecretManagerServiceClient} = require('@google-cloud/secret-manager');

const secretClient = new SecretManagerServiceClient();

/**
 * Retrieves the latest version of the named secret for the current GCP project.
 *
 * @param {string} name
 * @returns {Promise<string>}
 */
async function getSecret(name) {
  const path = `projects/${process.env.GCP_PROJECT}/secrets/${name}/versions/latest`;
  console.log(`Retrieving ${path} from Secret Manager...`)
  const [version] = await secretClient.accessSecretVersion({
    name: path,
  });
  return version.payload.data.toString('utf8');
}

/**
 * Download membership data to GCP storage.
 *
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.downloadMembershipData = async (req, res) => {
  // Pull these from environment variables to run locally.
  const username = await getSecret('membership_downloader_username');
  const password = await getSecret('membership_downloader_password');

  await memberships.downloadMembershipData(username, password);
};
