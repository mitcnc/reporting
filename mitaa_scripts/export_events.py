#!/usr/bin/python

"""
This script creates a CSV with the event registrations for a specified date.

The script accepts three parameters: the name/path of the file to be generated, and the start/end date for which to
query registrations. If no dates are provided, data will be exported for the current date.

Notes:
    1. All dates and times are UTC.
    2. The input dates should follow the ISO-8601 format (e.g., 2020-08-16 to represent August 16, 2020).

Examples:
    % GOOGLE_APPLICATION_CREDENTIALS=creds.json python export_events.py mitcnc_events_latest.csv
    % GOOGLE_APPLICATION_CREDENTIALS=creds.json python export_events.py mitcnc_events_20200816.csv 2020-08-16 2020-12-31
"""

import csv
import datetime
import logging
import sys
import typing
from typing import Dict, Union, cast

from google.cloud import bigquery

PROJECT_ID: str = 'mitcnc-membership'
DATASET_ID: str = 'reporting'
GROUP_ID: int = 25  # MITCNC
DEFAULT_REGISTRATION_COUNT: int = 1


def create_csv_row(db_row: Dict[str, Union[datetime.datetime, str]]) -> Dict[str, str]:
    field_aliases = {
        'advance_id': 'Constituent ID',
        'last_name': 'Last Name',
        'first_name': 'First Name',
        'event_id': 'control_id',
        'event_name': 'Control Name',
        'event_start': 'Event Start Date',
        'event_end': 'Event End Date',
        'email': 'Email Address',
        'registration_date': 'Registration Date',
        'attendee_id': 'Instance ID',
    }

    date_fields = ['event_start', 'event_end', 'registration_date', ]

    csv_row = {
        'Control Type': 'Event',
        'group_id': str(GROUP_ID),
        'Registration Count': str(DEFAULT_REGISTRATION_COUNT),
    }

    for db_field, csv_field in field_aliases.items():
        field_value = db_row[db_field]

        if db_field in date_fields and type(field_value) is datetime.datetime:
            field_value = cast(datetime.date, field_value).strftime('%m/%d/%y')

        csv_row[csv_field] = cast(str, field_value)

    return csv_row


def main(file: typing.TextIO, start_date: datetime.date, end_date: datetime.date):
    attendees_table_id = f'{PROJECT_ID}.{DATASET_ID}.attendees_annotated'
    events_table_id = f'{PROJECT_ID}.{DATASET_ID}.events'

    client = bigquery.Client()
    query = """
    SELECT
      a.id AS attendee_id,
      a.last_name,
      a.first_name,
      a.advance_id,
      a.email,
      a.created AS registration_date,
      e.id AS event_id,
      e.start AS event_start,
      e.`end` AS event_end,
      e.name AS event_name
    FROM
      `{0}` a
    JOIN
      `{1}`e
    ON
      a.event_id = e.id
    WHERE
      a.created BETWEEN '{2}' AND '{3}'
    ORDER BY
      a.created,
      a.advance_id
    """.format(attendees_table_id, events_table_id, start_date, end_date)
    query_job = client.query(query)

    field_names = [
        'Last Name',
        'First Name',
        'Billing Street 1',
        'Billing Street 2',
        'Billing City',
        'Billing State',
        'Billing Zip',
        'Billing Country',
        'Purchase Date',
        'Credit Card Type',
        'Billing Email',
        'Transaction ID',
        'Authorization Code',
        'group_id',
        'control_id',
        'Control Type',
        'Control Name',
        'Event Start Date',
        'Event End Date',
        'Constituent ID',
        'Email Address',
        'Registration Count',
        'Registration Date',
        'Instance ID',
        'Request ID',
        'Merchant ID',
        'Last Four Digits',
        'Billing Phone',
        'SAP account',
    ]

    writer = csv.DictWriter(file, fieldnames=field_names, quoting=csv.QUOTE_ALL)
    writer.writeheader()

    results = query_job.result()
    logging.info('Retrieved %d registrations from BigQuery', results.total_rows)

    for db_row in results:
        csv_row = create_csv_row(db_row)
        writer.writerow(csv_row)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

    output_filename = sys.argv[1]

    with open(output_filename, 'w') as output_file:
        start_date = datetime.date.today()
        if len(sys.argv) > 2:
            start_date = datetime.datetime.strptime(sys.argv[2], '%Y-%m-%d').date()
            end_date = start_date + datetime.timedelta(days=1)

            if len(sys.argv) > 3:
                end_date = datetime.datetime.strptime(sys.argv[3], '%Y-%m-%d').date()

        logging.info('Retrieving all event registrations from %s to %s', start_date, end_date)

        main(output_file, start_date, end_date)

        logging.info('Successfully wrote data as CSV to %s', output_filename)
