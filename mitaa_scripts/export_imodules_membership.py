#!/usr/bin/python

"""
This script creates a CSV that can be loaded into iModules. The CSV follows the
format at https://support.imodules.com/hc/en-us/articles/226201428-Importing-Data-into-a-Membership-Campaign.

The script accepts a single parameter which is the name/path of the file to be generated.
NOTE that filenames for MITCNC should end with "_sealedid25.csv" (e.g., export_sealedid25.csv);
however, this script does not enforce this. The name of the file will be whatever is passed in.
"""

import csv
import datetime
import sys
import typing
from typing import Dict, Union, cast

from google.cloud import bigquery

PROJECT_ID: str = 'mitcnc-membership'
DATASET_ID: str = 'reporting'
COMMUNITY_PREFIX: str = 'MEM - CNC'


def create_csv_row(db_row: Dict[str, Union[datetime.datetime, str]]) -> Dict[str, str]:
    field_aliases = {
        'advance_id': 'ADVANCE_ID',
        'level': f'{COMMUNITY_PREFIX} - Membership Level',
        'last_purchased_at': f'{COMMUNITY_PREFIX} - Purchase Date',
        'expires_at': f'{COMMUNITY_PREFIX} - Expiration Date',
    }

    date_fields = ['expires_at', 'last_purchased_at', ]

    csv_row = {
        f'{COMMUNITY_PREFIX} - Status': 'Current',
    }

    for db_field, csv_field in field_aliases.items():
        field_value = db_row[db_field]

        if db_field in date_fields and type(field_value) is datetime.datetime:
            field_value = cast(datetime.date, field_value).strftime('%m/%d/%Y %H:%M:%S')

        csv_row[csv_field] = cast(str, field_value)

    return csv_row


def main(file: typing.TextIO):
    table_id = f'{PROJECT_ID}.{DATASET_ID}.encompass_memberships_export'

    # Get all rows from the view
    client = bigquery.Client()
    query = """
        SELECT
          *
        FROM
          `{}`
        ORDER BY
          advance_id
        """.format(table_id)
    query_job = client.query(query)

    field_names = [
        'ADVANCE_ID',
        f'{COMMUNITY_PREFIX} - Membership Level',
        f'{COMMUNITY_PREFIX} - Purchase Date',
        f'{COMMUNITY_PREFIX} - Expiration Date',
        f'{COMMUNITY_PREFIX} - Status',
    ]

    writer = csv.DictWriter(file, fieldnames=field_names, quoting=csv.QUOTE_ALL)
    writer.writeheader()

    for db_row in query_job:
        csv_row = create_csv_row(db_row)
        writer.writerow(csv_row)


if __name__ == '__main__':
    with open(sys.argv[1], 'w') as output_file:
        main(output_file)
