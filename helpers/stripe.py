from dataclasses import dataclass, fields
from decimal import Decimal
from typing import Optional, Type, TypeVar

S = TypeVar('S', bound='Subscription')
InvoiceType = TypeVar('InvoiceType', bound='Invoice')


def parse_amount(amount: int) -> Decimal:
    return Decimal(amount) / 100


def maybe_parse_amount(amount: Optional[int]) -> Optional[Decimal]:
    return parse_amount(amount) if amount is not None else None


@dataclass
class Customer:
    id: str
    wordpress_user_id: Optional[int] = None
    name: Optional[str] = None
    email: Optional[str] = None


@dataclass
class Invoice:
    id: str
    status: str
    customer: str
    auto_advance: bool
    collection_method: str
    currency: str
    subtotal: Decimal
    total: Decimal
    paid: bool
    number: Optional[str] = None
    charge: Optional[str] = None
    description: Optional[str] = None
    payment_intent: Optional[str] = None
    period_start: Optional[int] = None
    period_end: Optional[int] = None
    subscription: Optional[str] = None
    subtotal_excluding_tax: Optional[Decimal] = None
    tax: Optional[Decimal] = None
    total_excluding_tax: Optional[Decimal] = None

    def __post_init__(self):
        # Filter out extra parameters
        for key, value in self.__dict__.items():
            if key not in [field.name for field in fields(self)]:
                delattr(self, key)

    @classmethod
    def from_api_object(cls: Type[InvoiceType], api_invoice: dict) -> InvoiceType:
        kwargs = {
            **api_invoice,
            **{
                'subtotal': parse_amount(api_invoice['subtotal']),
                'total': parse_amount(api_invoice['total']),
                'tax': maybe_parse_amount(api_invoice['tax']),
                'subtotal_excluding_tax': maybe_parse_amount(api_invoice['subtotal_excluding_tax']),
                'total_excluding_tax': maybe_parse_amount(api_invoice['total_excluding_tax']),
            }
        }

        # Remove unsupported fields
        allowed_fields = set([field.name for field in fields(cls)])
        fields_to_delete = set(kwargs.keys()) - allowed_fields
        for key in fields_to_delete:
            del kwargs[key]

        return cls(**kwargs)


@dataclass
class Subscription:
    id: str
    status: str
    customer: str
    cancel_at_period_end: bool
    start_date: int
    current_period_start: int
    current_period_end: int
    price_id: str
    price_name: str
    amount: Decimal
    created: int
    canceled_at: Optional[int] = None
    ended_at: Optional[int] = None
    trial_start: Optional[int] = None
    trial_end: Optional[int] = None
    schedule: Optional[str] = None
    cybersource_transaction_id: Optional[str] = None
    note: Optional[str] = None
    pause_collection__behavior: Optional[str] = None
    latest_invoice: Optional[str] = None

    @classmethod
    def from_api_object(cls: Type[S], api_subscription: dict) -> S:
        plan = api_subscription['plan']
        if not isinstance(plan, dict):
            raise ValueError('The `plan` field should be expanded when retrieving subscriptions from the Stripe API.')

        return cls(
            id=api_subscription['id'],
            created=api_subscription['created'],
            status=api_subscription['status'],
            customer=api_subscription['customer'],
            cancel_at_period_end=api_subscription['cancel_at_period_end'],
            start_date=api_subscription['start_date'],
            ended_at=api_subscription['ended_at'],
            canceled_at=api_subscription['canceled_at'],
            current_period_start=api_subscription['current_period_start'],
            current_period_end=api_subscription['current_period_end'],
            price_id=plan['id'],
            price_name=plan['product']['name'],
            trial_start=api_subscription['trial_start'],
            trial_end=api_subscription['trial_end'],
            amount=parse_amount(plan['amount']),
            schedule=api_subscription['schedule'],
            cybersource_transaction_id=api_subscription['metadata'].get('cybersource_id', None),
            note=api_subscription['metadata'].get('note', None),
            pause_collection__behavior=(api_subscription.get('pause_collection', {}) or {}).get('behavior', None),
            latest_invoice=api_subscription['latest_invoice'],
        )
