#!/usr/bin/python
from main import cancel_deceased_memberships
from reporting.bigquery import refresh_deceased_member_data


def main():
    refresh_deceased_member_data()
    cancel_deceased_memberships()


if __name__ == '__main__':
    main()
