#!/usr/bin/python
from main import post_process_subscriptions


def main():
    post_process_subscriptions()


if __name__ == '__main__':
    main()
