#!/usr/bin/python
from main import refresh_eventbrite_attendees, refresh_eventbrite_events


def main():
    refresh_eventbrite_events()
    refresh_eventbrite_attendees()


if __name__ == '__main__':
    main()
