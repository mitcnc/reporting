#!/usr/bin/python
from main import import_stripe_customers, import_stripe_invoices, import_stripe_subscriptions


def main():
    import_stripe_customers()
    import_stripe_invoices()
    import_stripe_subscriptions()


if __name__ == '__main__':
    main()
