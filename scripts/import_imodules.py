#!/usr/bin/python
import sys

from reporting import bigquery
from reporting.imodules import clean_membership_data
from reporting.models import destroy_database


def main():
    bigquery.is_connected()
    destroy_database()
    with open(sys.argv[1], 'r') as input_file:
        membership, purchases = clean_membership_data(input_file)
    bigquery.refresh_imodules_membership_data(membership)
    bigquery.refresh_membership_purchase_imodules_data(purchases)
    bigquery.create_memberships_view()
    bigquery.create_memberships_export_view()


if __name__ == '__main__':
    main()
