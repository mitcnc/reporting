const puppeteer = require('puppeteer');
const {Storage} = require('@google-cloud/storage');

const BUCKET_NAME = 'mitcnc-membership-purchases';
const DOWNLOAD_PATH = '/tmp/';

async function login(page, username, password) {
  console.log(`Logging in as ${username}...`);

  await page.goto('http://northerncalifornia.alumclub.mit.edu');
  await page.evaluate(() => {
    document.querySelector('#Login a').click();
  });
  await page.waitForNavigation();
  await page.type('#username', username);
  await page.type('#password', password);
  await page.click('[type="submit"]');

  console.log('Login completed!')
}

async function getActionLink(page, path, suffix) {
  const actionLinks = (await page.$x(path));

  for (const link of actionLinks) {
    const id = await (await link.getProperty('id')).jsonValue();

    if (id.endsWith(suffix)) {
      return link;
    }
  }
}

async function clickActionLink(page, path, suffix) {
  const linkHandle = await getActionLink(page, path, suffix);
  await page.evaluate(link => link.click(), linkHandle);
}

async function downloadReport(page) {
  console.log('Running report...');
  await page.goto('https://adminlb.imodules.com/admin/index.aspx?sid=1314&gid=25&cid=671&gfid=66&sr1=1&rtid=16');

  await clickActionLink(page, `//td[contains(., 'Data Dump')]/../td/a`, 'RunExport');
  await clickActionLink(page, `//td[contains(., 'Data Dump')]/../td/div/a`, 'RunWithRange');
  console.log('Report generation started!');

  console.log('Downloading report...');
  await page.waitForNavigation();
  const fileName = await page.evaluate(() => {
    const downloadLink = document.querySelector('#ctl02_lblFinish a');
    downloadLink.click();
    const fileNameRegex = /ExportResults\/(.*\.csv)/g;
    return fileNameRegex.exec(downloadLink.href)[1];
  });
  // NOTE: It is easier to just wait for how long we think the file
  // will take to download than do the various workarounds to monitor the
  // download and know exactly when it has finished.
  await page.waitForTimeout(7000);
  console.log(`Downloaded ${fileName}!`);

  return fileName;
}

async function storeReport(filePath) {
  const storage = new Storage();

  // Store file with UTC timestamp as name
  const newFileName = `${new Date().getTime()}.csv`;

  console.log(`Storing ${filePath} to GCP bucket ${BUCKET_NAME} as ${newFileName}...`);
  const bucket = storage.bucket(BUCKET_NAME);
  await bucket.upload(filePath, {destination: newFileName});
  console.log('File stored successfully!')
}

/**
 * Download membership data to GCP storage.
 *
 * This function uses Puppeteer to login to iModules, run a data export,
 * download a CSV, and upload the CSV to GCP storage.
 *
 * @param {string} username
 * @param {string} password
 * @returns {Promise<void>}
 */
exports.downloadMembershipData = async function (username, password) {
  const browser = await puppeteer.launch({
    // NOTE: Sandbox must be disabled for GCP
    args: ['--no-sandbox'],
    // NOTE: Uncomment to test locally
    // headless: false,
  });
  const page = await browser.newPage();
  await page._client().send('Page.setDownloadBehavior', {
    behavior: 'allow',
    downloadPath: DOWNLOAD_PATH,
  });

  await login(page, username, password);
  const fileName = await downloadReport(page);
  const filePath = `${DOWNLOAD_PATH}${fileName}`;
  await storeReport(filePath);

  await browser.close();
};
