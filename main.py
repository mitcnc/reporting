"""
This file contains functions that can be run on Google Cloud.

If these should also be run locally, please create a Python script in the
`scripts` directory.
"""
import io
import os
import tempfile

from google.cloud import secretmanager, storage  # type: ignore
from google.cloud.bigquery import Client

from membership_management.mit10 import MIT10Scheduler
from membership_management.pause_collection_for_lifetime_subscriptions import PauseCollectionForLifetimeSubscriptions
from reporting import bigquery
from reporting.eventbrite import EventbriteWrapper
from reporting.imodules import clean_membership_data
from reporting.models import db, destroy_database
from reporting.stripe import StripeImporter, cancel_subscription
from reporting.utils import dataclass_list_to_json
from reporting.wordpress import WordpressImporter


def refresh_eventbrite_events(event=None, context=None):  # pylint: disable=unused-argument
    """ Export events from Eventbrite, and load into BigQuery. """

    _assert_bigquery_connected()

    eventbrite = _setup_eventbrite()

    events = eventbrite.get_events()
    data_buffer = eventbrite.get_events_csv(events)
    bigquery.refresh_event_data(data_buffer)


def refresh_eventbrite_attendees(event=None, context=None):  # pylint: disable=unused-argument
    """ Export event attendees from Eventbrite, and load into BigQuery. """

    _assert_bigquery_connected()

    eventbrite = _setup_eventbrite()

    changed_since = bigquery.get_eventbrite_attendee_changed_since()
    print(f'Retrieving all Eventbrite attendees registered since {changed_since}')
    attendees = eventbrite.get_attendees(changed_since)
    buffer = dataclass_list_to_json(attendees)
    bigquery.refresh_attendee_data(buffer)
    bigquery.create_attendees_annotated_view()


def load_membership_data(data=None, context=None):  # pylint: disable=unused-argument
    """ Export memberships from iModules, and load into BigQuery. """

    _assert_bigquery_connected()

    # Ensure we can actually read the file from Storage
    bucket_name = data['bucket']
    file_name = data['name']
    input_file = _read_file_from_storage(bucket_name, file_name)

    # Recreate the temporary SQLite database
    destroy_database()
    db.connect()
    db.close()

    # Transform the data
    membership, purchases = clean_membership_data(input_file)

    # Load the data into BigQuery
    bigquery.refresh_imodules_membership_data(membership)
    bigquery.refresh_membership_purchase_imodules_data(purchases)


def import_stripe_customers(event=None, context=None):  # pylint: disable=unused-argument
    """ Import Stripe customers from Stripe into BigQuery. """
    _assert_bigquery_connected()

    importer = StripeImporter(_get_secret('stripe_api_key'))
    customers = importer.get_customers()

    buffer = dataclass_list_to_json(customers)
    bigquery.refresh_stripe_customer_data(buffer)


def import_stripe_invoices(event=None, context=None):  # pylint: disable=unused-argument
    """ Import Stripe invoices into BigQuery. """
    _assert_bigquery_connected()

    importer = StripeImporter(_get_secret('stripe_api_key'))
    invoices = importer.get_invoices()

    buffer = dataclass_list_to_json(invoices)
    bigquery.refresh_stripe_invoice_data(buffer)


def import_stripe_subscriptions(event=None, context=None):  # pylint: disable=unused-argument
    """ Import Stripe subscriptions into BigQuery. """
    print('Importing Stripe subscriptions into BigQuery...\n')

    _assert_bigquery_connected()

    importer = StripeImporter(_get_secret('stripe_api_key'))

    print('Downloading Stripe subscriptions...\n')
    subscriptions = importer.get_subscriptions()

    print('Loading data...\n')
    buffer = dataclass_list_to_json(subscriptions)
    bigquery.refresh_stripe_subscription_data(buffer)
    bigquery.create_stripe_memberships_view()
    bigquery.create_memberships_view()
    bigquery.create_memberships_export_view()


def cancel_deceased_memberships(event=None, context=None):  # pylint: disable=unused-argument
    """ Cancel Stripe subscriptions/memberships for deceased members. """
    _assert_bigquery_connected()

    stripe_api_key = _get_secret('stripe_api_key')

    query = """
    SELECT
      s.id,
      s.advance_id,
      s.level
    FROM
      `mitcnc-membership.reporting.memberships_stripe` s
    JOIN
      `reporting.deceased_members` dm
    ON
      dm.advance_id = s.advance_id
    WHERE
      s.level = 'Cardinal & Gray'
      AND (ended_at > CURRENT_TIMESTAMP()
        OR ended_at IS NULL)
    """
    client = Client()
    query_job = client.query(query)
    results = query_job.result()

    for row in results:
        advance_id = row.advance_id
        subscription_id = row.id
        try:
            cancel_subscription(stripe_api_key, subscription_id)
            print(f'Canceled subscription {subscription_id} for deceased member {advance_id}')
        except Exception as ex:  # pylint: disable=broad-except
            print(f'Failed to cancel subscription {subscription_id} for deceased member {advance_id}: {ex}')


def load_wordpress_users(event=None, context=None):  # pylint: disable=unused-argument
    """ Export users from WordPress, and load into BigQuery.

    WordPress holds the data we retrieve from MITAA during the OAuth login.
    """
    _assert_bigquery_connected()

    with tempfile.NamedTemporaryFile('w+t') as cert_file:
        with io.StringIO(_get_secret('wordpress_etl_db_ssl_certificate')) as cert_buffer:
            cert_file.write(cert_buffer.getvalue())
            cert_file.seek(0)

        importer = WordpressImporter(
            db_connection_string=_get_secret('wordpress_etl_db_connection_string'),
            db_ssl_cert_path=cert_file.name
        )
        users = importer.get_users()

    buffer = dataclass_list_to_json(users)
    bigquery.refresh_wordpress_user_data(buffer)


def post_process_subscriptions(event=None, context=None):  # pylint: disable=unused-argument
    """ Modify subscriptions after creation. """
    _assert_bigquery_connected()
    stripe_api_key = _get_secret('stripe_api_key')

    # Add a Stripe subscription schedule to MIT10 memberships so they properly convert
    # to normal memberships after the member has progressed out of MIT10.
    scheduler = MIT10Scheduler(
        stripe_api_key=stripe_api_key,
        mit10_price_id=os.environ['MIT10_PRICE_ID'],
        regular_price_id=os.environ['REGULAR_PRICE_ID']
    )
    scheduler.process()

    pause_processor = PauseCollectionForLifetimeSubscriptions(
        stripe_api_key=stripe_api_key,
        cardinal_and_gray_price_id=os.environ['CARDINAL_AND_GRAY_PRICE_ID']
    )
    pause_processor.process()


def _assert_bigquery_connected():
    if not bigquery.is_connected():
        raise RuntimeError('Cannot connect to BigQuery!')


def _setup_eventbrite():
    api_key = _get_secret('eventbrite_api_key')
    eventbrite = EventbriteWrapper(api_key)

    if not eventbrite.is_connected():
        raise RuntimeError('Cannot connect to Eventbrite!')

    return eventbrite


def _get_secret(name: str) -> str:
    print(f'Retrieving {name} from secret manager...')
    client = secretmanager.SecretManagerServiceClient()
    name = client.secret_version_path(os.environ['GCP_PROJECT'], name, 'latest')
    response = client.access_secret_version(request={"name": name})
    secret = response.payload.data.decode('UTF-8')
    return secret


def _read_file_from_storage(bucket_name: str, file_name: str) -> io.StringIO:
    """ Loads the specified file from GCP storage. """
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(bucket_name)
    blob = bucket.get_blob(file_name)
    bytes_file = io.BytesIO()
    blob.download_to_file(bytes_file)
    bytes_file.seek(0)
    return io.StringIO(bytes_file.read().decode('utf8'))
